# **Traffic Sign Recognition**

## Goals

The goals / steps of this project are the following:
* Load the data set (see below for links to the project data set)
* Explore, summarize and visualize the data set
* Design, train and test a model architecture
* Use the model to make predictions on new images
* Analyze the softmax probabilities of the new images
* Summarize the results with a written report


[//]: # (Image References)

[image1]: ./distribution.png "Distribution"
[image2]: ./signs-rescaled/267.png "Traffic Sign 1"
[image3]: ./signs-rescaled/209-30.png "Traffic Sign 2"
[image4]: ./signs-rescaled/206.png "Traffic Sign 3"
[image5]: ./signs-rescaled/274.png "Traffic Sign 4"
[image6]: ./signs-rescaled/306.png "Traffic Sign 5"

## Rubric Points
Here I will consider the [rubric points](https://review.udacity.com/#!/rubrics/481/view) individually and describe how I addressed each point in my implementation.

---
### Writeup / README

You're reading it!

### Data Set Summary & Exploration

#### 1. Provide a basic summary of the data set. In the code, the analysis should be done using python, numpy and/or pandas methods rather than hardcoding results manually.

I used python build in functions to calculate summary statistics of the traffic signs data set:

* The size of training set is 34799.
* The size of the validation set is 4410.
* The size of test set is 12630.
* The shape of a traffic sign image is (32, 32, 3).
* The number of unique classes/labels in the data set is 43.

#### 2. Include an exploratory visualization of the dataset.

Here is an exploratory visualization of the data set. It is a histogram showing how the data is distributed with respect to the unique classes. It is a very imbalanced distribution.

![alt text][image1]

### Design and Test a Model Architecture

#### 1. Describe how you preprocessed the image data. What techniques were chosen and why did you choose these techniques? Consider including images showing the output of each preprocessing technique. Pre-processing refers to techniques such as converting to grayscale, normalization, etc.


I normalized the image data by mean subtraction and dividing by standard deviation. Therefore the numerical values of the images are in a similar range. So they can be updated more consistently.


#### 2. Describe what your final model architecture looks like including model type, layers, layer sizes, connectivity, etc.) Consider including a diagram and/or table describing the final model.

My final model was a convolutional neural network with four convolutional layers:

| Layer         		|     Description	        					|
|:---------------------:|:---------------------------------------------:|
| Input         		| 32x32x3 RGB image   							|
| Convolution 5x5     	| 1x1 stride, same padding, outputs 32x32x16 	|
| RELU					|												|
| Max pooling	      	| 1x1 stride, same padding, outputs 32x32x16	|
| Convolution 3x3     	| 2x2 stride, same padding, outputs 16x16x32 	|
| RELU					|												|
| Max pooling	      	| 1x1 stride, same padding, outputs 16x16x32   	|
| Convolution 3x3     	| 1x1 stride, same padding, outputs 16x16x64 	|
| RELU					|												|
| Max pooling	      	| 1x1 stride, same padding, outputs 16x16x64	|
| Convolution 3x3     	| 1x1 stride, same padding, outputs 16x16x128 	|
| RELU					|												|
| Max pooling	      	| 1x1 stride, same padding, outputs 16x16x128  	|
| Flatten               | outputs 32768                                 |
| Fully connected       | outputs 256                                   |
| RELU					|												|
| Dropout               | Keep Probability of 0.7                       |
| Fully connected		| outputs 128       						    |
| RELU					|												|
| Dropout               | Keep Probability of 0.7                       |
| Fully connected       | outputs 43        							|



#### 3. Describe how you trained your model. The discussion can include the type of optimizer, the batch size, number of epochs and any hyperparameters such as learning rate.

To train the model, I used
- the Adam optimizer
- batch size of 128
- a number of epochs of 30
- a learning rate of 0.001

With a NVIDIA GTX 1080 Ti training required about 3 minutes.

#### 4. Describe the approach taken for finding a solution and getting the validation set accuracy to be at least 0.93. Include in the discussion the results on the training, validation and test sets and where in the code these were calculated. Your approach may have been an iterative process, in which case, outline the steps you took to get to the final solution and why you chose those steps. Perhaps your solution involved an already well known implementation or architecture. In this case, discuss why you think the architecture is suitable for the current problem.

My final model results were:
* validation set accuracy of 0.952.
* test set accuracy of 0.943.

My first attempt was to apply the LeNet architecture. I achieved a validation accuracy of about 0.90. For an image classification problem a convolutional network is generally appropiate. Therefore I decided to add more convolutional layers and to increase the depth of the convolutional layers. In this way the neural net is able to recognize more features in the images. In order to prevent the neural net from overfitting I applied dropout with a keep probability factor of 0.7. An attempt with a dropout of 0.5 was too aggressive. I ended up with the network described in detail above. The standard learning rate of 0.001 was appropiate for training the network.

### Test a Model on New Images

#### 1. Choose five German traffic signs found on the web and provide them in the report. For each image, discuss what quality or qualities might be difficult to classify.

Here are five German traffic signs that I downloaded from [here](http://www.auto-tipps.de/rechtliches/verkehrsschilder/) and rescaled to the size of 32 x 32.

![alt text][image2] ![alt text][image3] ![alt text][image4]

![alt text][image5] ![alt text][image6]

Since the images were downloaded from a catalogue with all german traffic signs the images are of excellent quality. Especially they have following qualities:
- the orientation of the traffic signs is the usual one
- the zoom level is optimal
- no partial occlusion
- no light reflection and no noise

On the downside you can argue that the captured images are generally not comparable with the traffic signs provided in the training data. This could cause false classifications by the neural net. Overall I don't believe so.

Furthermore the geometrical structure of the various signs is worth to be mentioned:
- No entry: the geometric pattern is unique, from this point of view the classification will be easy
- Go ahead: there might be an ambiguity with other direction signs such as turn left ahead or turn right ahead especially if the orientation of the sign is a little bit unusual in real life.
- Stop: the geometric pattern is unique, from this point of view the classification will be easy
- Speed limit: there are various speed limit signs. Therefore the network has to classify numbers. For this task the number of training signs with numbers might be too small. In this case speed limit signs were difficult to recognize.
- Priority road: again, the geometric pattern is unique, from this point of view the classification will be easy.


#### 2. Discuss the model's predictions on these new traffic signs and compare the results to predicting on the test set. At a minimum, discuss what the predictions were, the accuracy on these new predictions, and compare the accuracy to the accuracy on the test set.

Here are the results of the prediction:

| Image			        |     Prediction	        					|
|:---------------------:|:---------------------------------------------:|
| No entry      		| No entry									|
| Ahead only   			| Ahead only								|
| Stop				    | Stop										|
| Speed limit (60 km/h) | Speed limit (60 km/h)						|
| Priority Road			| Priority road 							|


The model was able to correctly guess 5 of the 5 traffic signs, which gives an accuracy of 100%. This compares favorably to the accuracy on the test set of 0.943. The new signs may be easier to recognize due to the lack of an environment.

#### 3. Describe how certain the model is when predicting on each of the five new images by looking at the softmax probabilities for each prediction. Provide the top 5 softmax probabilities for each image along with the sign type of each probability.

For the first image, the model is absolutely sure that this is a no entry sign (probability of 1.0). Indeed the image does contain a no entry sign. The top five soft max probabilities were

| Probability         	|     Prediction	        					|
|:---------------------:|:---------------------------------------------:|
| 1.0        		| No entry   										|
| 4.5e-25   		| Speed limit (20 km/h)								|
| 0					| Speed limit (30 km/h)								|
| 0	      			| Speed limit (50 km/h)		 						|
| 0				    | Speed limit (60 km/h)								|


For the second image, the model is absolutely sure that this is an ahead only sign (probability of 1.0). Indeed the image does contain an ahead only sign. The top five soft max probabilities were

| Probability         	|     Prediction	        					|
|:---------------------:|:---------------------------------------------:|
| 1.0        		| Ahead only  										|
| 5.1e-22   		| Turn right ahead						 			|
| 4.8e-25			| Go straight or right								|
| 1.5e-29      		| Keep right	 									|
| 1.4e-29			| Turn left ahead									|


For the third image, the model is relativly sure that this is a stop sign (probability of 0.719). Indeed the image does contain a stop sign. The top five soft max probabilities were

| Probability         	|     Prediction	        					|
|:---------------------:|:---------------------------------------------:|
| 1.0        		| Stop 												|
| 1.4e-14 			| Speed limit (80 km/h)					 			|
| 4.5e-16			| No vehicles										|
| 4.1e-17    		| Bycycles crossing 								|
| 8.8e-20			| Vehicles over 3.5 metric tons prohibited			|


For the fourth image, the model is relativly sure that this is a speed limit (60 km/h) sign (probability of 0.719). Indeed the image does contain a speed limit (60 km/h) sign. The top five soft max probabilities were

| Probability         	|     Prediction	        					|
|:---------------------:|:---------------------------------------------:|
| 0.719        		| Speed limit (60 km/h)												|
| 0.280 			| No passing				 			|
| 4.9e-4			| Children crossing										|
| 1.4e-6     		| Dangerous curve to the right						|
| 3.1e-8			| Vehicles over 3.5 metric tons prohibited			|


For the fifth image, the model is absolutely sure that this is a priority road sign (probability of 1.0). Indeed the image does contain a priority road sign. The top five soft max probabilities were

| Probability         	|     Prediction	        					|
|:---------------------:|:---------------------------------------------:|
| 1.0        		| Priority road 									|
| 5.0e-8 			| Keep right			 		        			|
| 1.5e-9			| Go straight or right								|
| 2.7e-11    		| Yield												|
| 1.3e-14			| End of no passing									|

It turns out that the most uncertain sign which the model recognizes is a speed limit sign.





